<?php

namespace PHPGit\Command;

use PHPGit\Command;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CleanCommand extends Command {

    public function __invoke(array $options = array()) {
        $options = $this->resolve($options);
        $builder = $this->git->getProcessBuilder()
            ->add("clean");

        $this->addFlags($builder, $options, array("force"));

        if ($options["directories"]) {
            $builder->add("-d");
        }

        $this->git->run($builder->getProcess());

        return true;
    }

    public function setDefaultOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            "force" => true,
            "directories" => true
        ));
    }

}